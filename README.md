#Air Hockey#

A simple air hockey game, designed mostly to gain familiarity with Unity3D.

Remember that things other than source or configuration (e.g., Prefabs, Scenes, Materials) do not merge. If you plan to collaborate with someone, changes to those types of files need to be synchronized and well communicated.