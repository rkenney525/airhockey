﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PaddleController : MonoBehaviour {

    private const float MOVEMENT_MODIFIER = 20.0f;

    private Rigidbody rigidBody;

    void Start() {
        this.rigidBody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        // Get the mouse movements
        float mouseXChange = Input.GetAxis("Mouse X");
        float mouseYChange = Input.GetAxis("Mouse Y");

        // Set the paddle position
        this.rigidBody.velocity = this.CalculateForce(mouseXChange, mouseYChange);
    }

    private Vector3 CalculateForce(float mouseXChange, float mouseYChange) {
        // Init
        Vector3 force = this.rigidBody.velocity;

        // Set the X and Z forces
        force.x = (MOVEMENT_MODIFIER * mouseXChange);
        force.z = (MOVEMENT_MODIFIER * mouseYChange);

        // Return
        return force;
    }
}
